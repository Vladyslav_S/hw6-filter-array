"use strict";

/* 1. Опишите своими словами как работает цикл forEachthis.
В метод ForEach передается функция, в которой укзаано что именно необходимо сделать с каждым элементом в масиве. Результат возвращется. */

function filterBy(arr, type) {
  let res = [];
  arr.forEach((value) => {
    if (typeof value !== type) {
      res.push(value);
    }
  });
  return res;
}

const arr1 = ["hello", "world", 23, "23", null];

const res = filterBy(arr1, "number");
console.log(res); // [23, null]
